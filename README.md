# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://aljazerzin@bitbucket.org/aljazerzin/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/aljazerzin/stroboskop/commits/1ebe56df9b440995130f02999c17b3b0c516c50f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aljazerzin/stroboskop/commits/b7571cf3aebcf127733bad5d5717a7669b2bb744?at=Izgled

Naloga 6.3.2:
https://bitbucket.org/aljazerzin/stroboskop/commits/5c7d9784152e265e1ff57ec036fcae00c055b871?at=Izgled

Naloga 6.3.3:
https://bitbucket.org/aljazerzin/stroboskop/commits/047b15dbcee7b846265b152798c9f71c420d7f04?at=Izgled

Naloga 6.3.4:
https://bitbucket.org/aljazerzin/stroboskop/commits/5da6c258eceab3b0189deb65090c989af18b7fc3

Naloga 6.3.5:

git checkout master
git merge Izgled
git push origin master
## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aljazerzin/stroboskop/commits/6e08f7791a218433314de550cb027968c15905b0

Naloga 6.4.2:
https://bitbucket.org/aljazerzin/stroboskop/commits/5f083bfcb46b599c158f3200e2784384477f1cf8

Naloga 6.4.3:
https://bitbucket.org/aljazerzin/stroboskop/commits/19435badf1e8eef7737e01334cf68082c8383d13

Naloga 6.4.4:
https://bitbucket.org/aljazerzin/stroboskop/commits/570239afae38ea4256375c256996e8cce7ad222e